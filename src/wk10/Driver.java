package wk10;

import javax.swing.*;
import java.util.ArrayList;

public class Driver {

   public static void simple(int number) {
       for(int i=1; i<number; ++i) {
           for(int j=0; i*j%2==0; ++j) {
               System.out.println("yo, yo");
           }
       }
   }




























    public static void main(String[] args) {
        ArrayList<Integer> data = new ArrayList<>();
        do {
            int value = Integer.parseInt(JOptionPane.showInputDialog("Enter a number"));
            data.add(value);
        } while(extractEvens(data).size()<3);
        JOptionPane.showMessageDialog(null, "The largest of the numbers entered is: " + maximum(data));
    }

    private static int maximum(ArrayList<Integer> values) {
        int maximum = Integer.MIN_VALUE;
        for(int value : values) {
            maximum = Math.max(value, maximum);
        }
        return maximum;
    }

    private static ArrayList<Integer> extractEvens(ArrayList<Integer> numbers) {
        ArrayList<Integer> evens = new ArrayList<>();
        for(int number : numbers) {
            if(number%2==0) {
                evens.add(number);
            }
        }
        return evens;
    }

}
