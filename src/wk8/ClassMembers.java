package wk8;

public class ClassMembers {
    private int instanceVariable;
    private static int classVariable;

    public static void main(String[] args) {
        ClassMembers c = new ClassMembers();
        c.instanceMethod();
    }

    public static int classMethod() {
        // have access to classVariable
        System.out.println(classVariable);
        // Do not have direct access to instanceVariable
        ClassMembers c = new ClassMembers();
        System.out.println(c.instanceVariable);
        // have access to classMethod
        // Do not have access to instanceMethod
        return 0;
    }

    public int instanceMethod() {
        // have access to classVariable
        System.out.println(this.classVariable);
        // have access to instanceVariable
        System.out.println(this.instanceVariable);
        // have access to classMethod
        classMethod();
        // have access to instanceMethod
        instanceMethod();

        this.equals(null);
        return 0;
    }
}
