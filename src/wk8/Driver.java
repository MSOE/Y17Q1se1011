package wk8;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        int[] quizScores;
        System.out.println("How many quizzes should you have taken?");
        Scanner in = new Scanner(System.in);
        int numberOfQuizzes = in.nextInt();
        quizScores = new int[numberOfQuizzes];
        for(int i=0; i<quizScores.length; ++i) {
            System.out.println("enter score for quiz " + (i+1));
            quizScores[i] = in.nextInt();
        }
        double average = calculateAverage(quizScores);
        System.out.println(average);


    }

    private static double calculateAverage(int[] quizScores) {
        int sum = 0;
        int lowestScore = Integer.MAX_VALUE;
        for(int i=0; i<quizScores.length; ++i) {
            sum += quizScores[i];
            lowestScore = Math.min(lowestScore, quizScores[i]);
        }
        sum -= lowestScore;
        return (double)sum/quizScores.length;
    }

    public static void main2(String[] args) {
        System.out.println("Enter your quiz scores, please");
        Scanner in = new Scanner(System.in);
        int q1 = in.nextInt();
        int q2 = in.nextInt();
        int q3 = in.nextInt();
        int q4 = in.nextInt();
        int q5 = in.nextInt();
        int q6 = in.nextInt();
        int q7 = in.nextInt();
        double average = calculateAverage(q1, q2, q3, q4, q5, q6, q7);
        System.out.println(average);
    }

    private static double calculateAverage(int q1, int q2, int q3, int q4, int q5, int q6, int q7) {
        int smallest = q1;
        smallest  = Math.min(smallest, q2);
        smallest  = Math.min(smallest, q3);
        smallest  = Math.min(smallest, q4);
        smallest  = Math.min(smallest, q5);
        smallest  = Math.min(smallest, q6);
        smallest  = Math.min(smallest, q7);
        return (double)(q1 + q2 + q3 + q4 + q5 + q6 + q7 - smallest)/6;
    }
}
