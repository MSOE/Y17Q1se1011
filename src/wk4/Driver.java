package wk4;

public class Driver {
    public static void main(String[] args) {
        System.out.println(Math.PI);
        System.out.println(Math.abs((double)3));
        System.out.println(Math.abs(-3));
        System.out.println(Math.cos(Math.toRadians(90)));

        // replace if(x==72.0) with something that is true if
        // x is within 1.0 of 72.0.
        double x = 71.3;
        if(Math.abs(72.0-x)<0.00001) {

        }
    }
}
