package wk5;

public class Student {
    private int studentID;
    private String firstName;
    private String lastName;
    private final String dateOfBirth;

    private static final int MAX_ID = 99999;

    public Student() {
        studentID = 0;
        firstName = "Not";
        lastName = "Born";
        dateOfBirth = "";
    }

    public Student(String name, String dateOfBirth, int id) {
        int MAX_ID = 4;
        firstName = name.substring(0, name.indexOf(" "));
        lastName = name.substring(name.indexOf(" ")+1);
        this.dateOfBirth = dateOfBirth;
        if(id<=Student.MAX_ID) {
            studentID = id;
        } else {
            studentID = 1;
        }
    }

    public String toString() {
        return firstName + " " + lastName + " born on " + dateOfBirth;
    }

    public int age() {
        final int currentYear = 2016;
        String year = dateOfBirth.substring(dateOfBirth.length()-4);
        int birthYear = Integer.parseInt(year);
        return currentYear - birthYear;
    }

    // Accessor
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    // Mutator
    public void setStudentID(int id) {
        studentID = id;
    }
}
