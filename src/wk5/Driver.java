package wk5;

public class Driver {
    public static void main(String[] args) {
        Student john = new Student("John Dough", "04/20/1995", 123);
        Student jane = new Student("Jane Dough", "04/20/1990", 124);
        int age = john.age();
        //john.setDateOfBirth("yikes");
        System.out.println(john.toString());
        System.out.println(jane.toString());
        System.out.println(john.age());
        System.out.println(jane.age());
    }
}
