package wk7;

import wk6.Complex;
import java.util.Random;
import java.util.Scanner;

public class ComplexQuiz {
    public static final int ADDITION = 1;
    public static final int SUBTRACTION = 2;
    public static final int MULTIPLICATION = 3;
    public static final int DIVISION = 4;
    public static final int MIXTURE = 5;

    public static void main(String[] args) {
        // Welcome user
        System.out.println("Welcome to the complex quizzer.  I'm here to help you with your complex math skills.");

        // Select quiz type (adding, subtracting, multiplying, dividing, or mixed)
        System.out.println("Select an area to focus on:\n 1. Adding\n" +
                " 2. Subtracting\n 3. Multiplying\n 4. Dividing\n 5. All of the above\n" +
                " 6. None of the above");
        Scanner in = new Scanner(System.in);
        int userSelection = 6;
        if(in.hasNextInt()) {
            userSelection = in.nextInt();
        }
        in.nextLine();

        // Present five questions
        int countCorrect = 0;
        Random magicHat = new Random();
        for(int i=0; i<5; ++i) {
            Complex a = new Complex(magicHat.nextInt(20)-10);
            Complex b = new Complex(magicHat.nextInt(20)-10);
            Complex c;
            int mathType = userSelection;
            if(mathType==MIXTURE) {
                mathType = magicHat.nextInt(4) + 1;
            }
            String mathSymbol;
            switch(mathType) {
                case ADDITION:
                    c = a.plus(b);
                    mathSymbol = "+";
                    break;
                case SUBTRACTION:
                    c = a.minus(b);
                    mathSymbol = "-";
                    break;
                case MULTIPLICATION:
                    c = a.times(b);
                    mathSymbol = "*";
                    break;
                default:
                    c = a.dividedBy(b);
                    mathSymbol = "/";
                    break;
            }
            System.out.println(a + " " + mathSymbol + " " + b + " = ?");
            Complex answer = new Complex(in.nextLine());
            if(answer.equals(c)) {
                System.out.println("Correct");
                ++countCorrect;
            } else {
                System.out.println("Oops!  The correct answer is: " + c);
            }
        }

        // Show results
        if(userSelection>0 && userSelection<MIXTURE) {
            System.out.println("You got " + countCorrect + " out of 5 correct.");
        }

    }
}
