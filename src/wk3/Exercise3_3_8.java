package wk3;

import java.util.Scanner;

public class Exercise3_3_8 {
    public static void main(String[] args) {
        System.out.println("Enter as many positive integers as you would like, followed by a non-positive integer");

        int sum = 0;
        int value;
        Scanner in = new Scanner(System.in);

        do {
            value = in.nextInt();
            if(value>0 && value%3==0) {
                sum += value;
            }
        } while(value>0);

        System.out.println(sum);
    }
}
