package wk3;

import com.sun.org.apache.xpath.internal.SourceTree;

import javax.swing.*;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        /*
        while(CONDITIONAL) {
            STATEMENT1;  // runs while CONDITIONAL is true
            STATEMENT2;  // runs while CONDITIONAL is true
        }
         */
        {
            int i = 5;
            while (i >= 0) {
                System.out.println(i + " * " + i + " = " + i * i);
                i--;
            }
        }
        /*
        for(INITIALIZATION; CONDITIONAL; UPDATE) {
            STATEMENT1;  // runs while CONDITIONAL is true
            STATEMENT2;  // runs while CONDITIONAL is true
        }
         */
        for(int i=5; i>=0; i--) {
            System.out.println(i + " * " + i + " = " + i*i);
        }
        /*
        do {
            STATEMENT1;  // runs once and then while CONDITIONAL is true
            STATEMENT2;  // runs once and then while CONDITIONAL is true
        } while(CONDITIONAL);

        //equivalent to:

        STATEMENT1;  // runs once
        STATEMENT2;  // runs once
        while(CONDITIONAL) {
            STATEMENT1;  // runs while CONDITIONAL is true
            STATEMENT2;  // runs while CONDITIONAL is true
        }
         */
    }




    public static void main4(String[] args) {
        String optionSelected = JOptionPane.showInputDialog(null, "Enter an option.\n 1. Option one\n" +
        " 2. Option two\n 3. Option three");
        int option = Integer.parseInt(optionSelected);
        switch (option) {
            case 1:
                JOptionPane.showMessageDialog(null, "Here is option one");
                break;
            case 2:
                JOptionPane.showMessageDialog(null, "Here is option two");
                break;
            case 3:
                JOptionPane.showMessageDialog(null, "Here is option three");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Get with the program");
        }
    }






    public static void main3(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your grade (as an integer)");
        int grade = in.nextInt();
        char letterGrade = 'F';
        switch (grade) {
            case 100: case 99: case 98: case 97: case 96: case 95:
            case 94:  case 93: case 92: case 91: case 90:
                letterGrade = 'A';
                break;
            case 89:
                letterGrade = 'B';
                break;
        }
/*        if(grade>=90) {
            letterGrade = 'A';
        } else if(grade>=80) {
            letterGrade = 'B';
        } else if(grade>=70) {
            letterGrade = 'C';
        } else if(grade>=60) {
            letterGrade = 'D';
        }
*/
        System.out.println("your letter grade is: " + letterGrade);
    }


    public static void main2(String[] args) {
        /*
        if(CONDITIONAL) {
            STATEMENT1;  // runs if CONDITIONAL is true
            STATEMENT2;  // runs if CONDITIONAL is true
        } else {
            STATEMENT1;  // runs if CONDITIONAL is false
            STATEMENT2;  // runs if CONDITIONAL is false
        }
         */
        String phrase = JOptionPane.showInputDialog(null, "Enter a phrase");
        if (phrase.length() <= 10) {
            if (phrase.length() == 5 || phrase.charAt(0)=='d') {
                JOptionPane.showMessageDialog(null, "you're lucky");
            } else {
                JOptionPane.showMessageDialog(null, "You're lazy");
            }
        //} else if (phrase.charAt(0) != 'd' || phrase.charAt(0) != 'D') {
        } else if (phrase.toUpperCase().charAt(0) != 'D') {
            JOptionPane.showMessageDialog(null, "Wow, what long fingers you have");
        } else {
            JOptionPane.showMessageDialog(null, "You're lucky");
        }
    }
}





