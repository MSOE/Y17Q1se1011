package wk3;

import javax.swing.*;

public class Exercise3_3_4 {
    public static void main(String[] args) {
        String word = JOptionPane.showInputDialog(null, "Enter a word");

        boolean isAlphabetical = true;
        boolean isReverseAlphabetical = true;

        for(int i=0; i<word.length()-1; ++i) {
            int difference = word.charAt(i) - word.charAt(i+1);
            if(difference>0) {
                isAlphabetical = false;
            }
            if(difference<0) {
                isReverseAlphabetical = false;
            }
        }
        if(isAlphabetical) {
            JOptionPane.showMessageDialog(null, "The letters entered are in alphabetical order.");
        } else if(isReverseAlphabetical) {
            JOptionPane.showMessageDialog(null, "The letters entered are in reverse alphabetical order.");
        } else {
            JOptionPane.showMessageDialog(null, "The letters entered are not in order.");
        }
    }
}
