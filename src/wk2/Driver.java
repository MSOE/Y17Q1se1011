package wk2;

import javax.swing.*;
import java.util.Scanner;

/**
 * Created by taylor on 9/12/2016.
 */
public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter two words");
        String a = in.next();
        String b = in.next();
        String result = a + b + b + a;
        System.out.println(result);
    }




    public static void main5(String[] args) {
        String num = JOptionPane.showInputDialog("Enter your age");
        JOptionPane.showMessageDialog(null, "Next year at this time, you will be "
                + (Integer.parseInt(num) + 1));
    }

    public static void main4(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your age");
        int num = in.nextInt();
        System.out.println("Next year at this time, you will be " + (num + 1));
    }





    public static void main3(String[] args) {
        char z;       // declared
        int a = 1;    // initialized
        int b = (int)5.1;
        b = 5;        // assignment
        int c = 8;
        double y = 8;
        double x = (double)b / c; // automatic type promotion
        x += 6.6;         // x = x + 6.6;
        System.out.println(x + 2); // 8.6
        System.out.println(a + b); // 6
        System.out.println(a - b); // -4
        System.out.println(b * c); // 40
        System.out.println(b / c); // 0
        System.out.println(b % c); // 5
        y -= a + b;    // 8 -= 1 + 5
        System.out.println(y);     // 2
    }







    public static void main2(String[] args) {
        String word = "recoil";
        System.out.println(word);
        int beginIndex = 2;
        System.out.println(word.substring(beginIndex));
        System.out.println(word.substring(2, word.length()));
        System.out.println(word.substring(2, 5) + "n");
    }
}
