/*
 Block comment
 */
package wk1;

import java.util.Scanner;

/**
 * Javadoc comment
 */
public class Driver {
    public static void main(String[] args) {
        int i = 13;
        long l = 1399999999999L; // forget this soon
        float f = 3.14F; // forget this soon
        double d = 3.14;
        char c = 'h';
        boolean b = true;
        Scanner in = new Scanner(System.in);
        Scanner inner = in;
        char cc = c;
        String name = "Billy Bob";
        name = in.nextLine();

        int length = name.length();
        System.out.println(name.charAt(length-2));

//        System.out.print("something to print");
//        System.out.println(" " + i + l + f + d + c);
    }
}
