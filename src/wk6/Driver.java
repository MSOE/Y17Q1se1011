package wk6;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a complex number in the form: 3.0 + 4.3i");
        Complex c1 = new Complex(in.nextLine());
        System.out.println("Enter a complex number in the form: 3.0 + 4.3i");
        Complex c2 = new Complex(in.nextLine());

        System.out.println(c1.toString() + " + " + c2.toString() + " = " + c1.plus(c2).toString());
    }
}
