package wk9;

/* the foreach loop
     double[] numbers = {1, 2, 3};
     for(double number : numbers) {
         System.out.println(number);
     }
     for(int i=0; i<numbers.length; ++i) {
         double number = numbers[i];
         System.out.println(number);
     }
 * copying arrays
   - making a new array (of the same size)
   - looping through the old array, copying each element into the new array
 * two dimensional arrays
     Tile[][] gameBoard = new Tile[4][4];
 * searching for items in arrays
   - loop through elements
   - [smile] if you find what you're looking for
 */
public class Riley {
    public static void main(String[] args) {
        int[] quizScores = {7, 3, 8, 4, 3, 9, 10};
        System.out.println("Quiz competent of grade: " + calculateQuizGrade(quizScores));
    }

    private static double calculateQuizGrade(int[] quizScores) {
        int[] realQuizScores = removeLowest(quizScores);
        return calculateAverage(realQuizScores);
    }

    private static double calculateAverage(int[] data) {
        int sum = 0;
        for(int number : data) {
            sum += number;
        }
        return (double)sum/data.length;
    }

    private static int[] removeLowest(int[] data) {
        int[] result = new int[data.length-1];
        int lowest = Integer.MAX_VALUE;
        for(int num : data) {
            if(lowest>num) {
                lowest = num;
            }
        }
        boolean haveRemovedLowest = false;
        for(int i=0; i<data.length; ++i) {
            if(!haveRemovedLowest) {
                if (data[i] != lowest) {
                    result[i] = data[i];
                } else {
                    haveRemovedLowest = true;
                }
            } else {
                result[i-1] = data[i];
            }
        }
        return result;
    }


}





















