package wk9;

import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        int[] nums = {3, 8, 4, 5, 2};
        ArrayList<Integer> result = extractEvensArrayList(nums);
        System.out.println(nums);
        result.remove(new Integer(2));
        System.out.println(result);
    }

    private static ArrayList<Integer> extractEvensArrayList(int[] nums) {
        ArrayList<Integer> evens = new ArrayList<>();
        for(int number : nums) {
            if(number%2==0) {
                evens.add(number);
            }
        }
        System.out.println(evens.get(2));
        return evens;
    }

    public static void main2(String[] args) {
        int[] nums = {3, 2, 4, 5, 8};
        int[] result = extractEvens(nums);
    }

    private static int[] extractEvens(int[] nums) {
        int[] evens = new int[countEvens(nums)];
        int evenIndex = 0;
        for(int i=0; i<nums.length && evenIndex<evens.length; ++i) {
            if(nums[i]%2==0) {
                evens[evenIndex] = nums[i];
                evenIndex++;
            }
        }
        return evens;
    }

    private static int countEvens(int[] nums) {
        int evenCount = 0;
        for(int number : nums) {
            if(number%2==0) {
                evenCount++;
            }
        }
        return evenCount;
    }
}
